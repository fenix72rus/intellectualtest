using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TimeSetterController : MonoBehaviour
{
    public Action OnTimeValuesSetted;

    [SerializeField] private Button _doneButton;
    [SerializeField] private RectTransform _settingsPanel;
    [SerializeField] private List<TaskTimeChangerView> _timeChangerViews;

    private List<GameModeSettings> _gameModeSettings;

    public void InitSettings(List<GameModeSettings> settings)
    {
        _gameModeSettings = settings;

        UpdateViews();
    }

    private void UpdateViews()
    {
        for(int i = 0; i < _gameModeSettings.Count; i++)
        {
            if (i >= _timeChangerViews.Count)
                continue;

            var setting = _gameModeSettings[i];
            var view = _timeChangerViews[i];

            view.RememberDuration.Value = (int)setting.RememberDurationInSeconds;
            view.GameDuration.Value = (int)setting.ResponseTimeInSeconds;
        }
    }

    private void Start()
    {
        _doneButton.onClick.AddListener(ProcessButtonClick);
    }

    private void ProcessButtonClick()
    {
        SetEnteredValues();
        _settingsPanel.gameObject.SetActive(false);

        OnTimeValuesSetted?.Invoke();
    }

    private void SetEnteredValues()
    {
        for(int i = 0; i < _timeChangerViews.Count; i++)
        {
            var setting = _gameModeSettings[i];
            var view = _timeChangerViews[i];

            setting.RememberDurationInSeconds = view.RememberDuration.Value;
            setting.ResponseTimeInSeconds = view.GameDuration.Value;
        }
    }
}
