using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskTimeChangerView : MonoBehaviour
{
    public TimeInputField RememberDuration => _rememberDurationInput;
    public TimeInputField GameDuration => _gameDurationInput;

    [SerializeField] private TimeInputField _rememberDurationInput, _gameDurationInput;
}
