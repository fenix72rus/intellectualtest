using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeInputField : MonoBehaviour
{
    public int Value
    {
        get
        {
            int.TryParse(_inputField.text, out var result);

            return result;
        }
        set
        {
            _inputField.text = value.ToString();
        }
    }

    [SerializeField] private TMP_InputField _inputField;
}
