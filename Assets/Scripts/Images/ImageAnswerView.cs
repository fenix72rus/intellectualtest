using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ImageAnswerView : AnswerView<Texture2D>
{
    public Action<Texture2D, ImageAnswerView> OnSelected;
    [SerializeField] private RectTransform _selectedBorder;

    [SerializeField] private RawImage _rawImage;

    private Vector3 _startScale;

    public override void InitValue(Texture2D value)
    {
        base.InitValue(value);
        _rawImage.texture = value;
    }

    public override void SetSelectVisual()
    {
        _rawImage.rectTransform.localScale = _startScale * 0.85f;
        _selectedBorder.gameObject.SetActive(true);
    }

    public override void SetDeselectVisual()
    {
        _rawImage.rectTransform.localScale = _startScale;
        _selectedBorder.gameObject.SetActive(false);
    }

    protected override void ProcessAnswerClick()
    {
        OnSelected?.Invoke(_answerValue, this);
    }

    protected override void InitComponents()
    {
        base.InitComponents();
        _startScale = _rawImage.rectTransform.localScale;
    }
}
