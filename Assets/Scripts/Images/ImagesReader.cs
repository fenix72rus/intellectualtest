using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ImagesReader 
{
    private string _imagesFolderPath;

    public ImagesReader(string assetsFolder)
    {
        _imagesFolderPath = Path.Combine(Application.streamingAssetsPath, assetsFolder);
    }

    public List<Texture2D> GetImages()
    {
        if (!Directory.Exists(_imagesFolderPath))
            throw new System.Exception($"Has no exists directory - {_imagesFolderPath}");

        var imageFiles = GetImageFiles();

        var images = GetImagesFromFiles(imageFiles);

        return images;
    }

    private List<Texture2D> GetImagesFromFiles(string[] files)
    {
        var images = new List<Texture2D>();

        foreach (var file in files)
        {
            var image = GetImageFromPath(file);

            images.Add(image);
        }

        return images;
    }

    private Texture2D GetImageFromPath(string path)
    {
        if (!File.Exists(path))
            throw new System.Exception($"Has no exists image file - {path}");

        var textureBytes = File.ReadAllBytes(path);
        var texture = new Texture2D(2, 2);
        texture.LoadImage(textureBytes);

        return texture;
    }

    private string[] GetImageFiles()
    {
        var jpgFiles = Directory.GetFiles(_imagesFolderPath, "*.jpg");
        var pngFiles = Directory.GetFiles(_imagesFolderPath, "*.png");
        var imageFiles = new string[jpgFiles.Length + pngFiles.Length];

        jpgFiles.CopyTo(imageFiles, 0);
        pngFiles.CopyTo(imageFiles, jpgFiles.Length);

        return imageFiles;
    }
}
