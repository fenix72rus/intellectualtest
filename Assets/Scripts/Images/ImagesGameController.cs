using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImagesGameController : GameModeController<Texture2D>
{
    private ImagesGameView _gameView;

    public ImagesGameController(GameMode<Texture2D> gameMode, GameModeView gameModeView, float rememberDuration, float playStateDuration)
        : base(gameMode, gameModeView, rememberDuration, playStateDuration)
    {
        _gameView = gameModeView as ImagesGameView;
        _gameView.OnAnswerSelected += ProcessSelectAnswer;
    }

    public override void StartGame()
    {
        Debug.Log($"Right answers count {_gameMode.RightAnswers.Count}/All answers Count {_gameMode.AllAnswers.Count}");
        _gameView.Init(_gameMode.RightAnswers, _gameMode.AllAnswers);

        base.StartGame();
    }
}
