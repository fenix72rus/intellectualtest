using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ImagesGameView : GameModeView
{
    public Action<Texture2D, ImageAnswerView> OnAnswerSelected;

    [SerializeField] private ImageAnswerView _answerPrefab;
    [SerializeField] private List<ImageAnswerView> _answersViews = new List<ImageAnswerView>();
    [SerializeField] private List<Texture2D> _rightAnswers, _allAnswers;

    public void Init(List<Texture2D> rightAnswers, List<Texture2D> allAnswers)
    {
        _answersViews.Clear();
        _allAnswers.Clear();
        _rightAnswers.Clear();

        _rightAnswers = rightAnswers;
        _allAnswers = allAnswers;
        Init();
    }

    public override void EnableAnswerInteracts(bool status)
    {
        foreach (var answer in _answersViews)
        {
            answer.EnableInteract(status);
        }
    }

    public override void ClearRememberPanel()
    {
        RemoveAllChildrens(_rememberPanel);
    }

    public override void ClearAnswersPanel()
    {
        RemoveAllChildrens(_answersPanel);
    }

    public override void EnableRightAnswersBorder()
    {
        foreach (var rightValue in _rightAnswers)
        {
            foreach (var answerView in _answersViews)
            {
                if (answerView.Value == rightValue)
                {
                    answerView.EnableRightBorder();
                    break;
                }
            }
        }
    }

    protected override void FillAnswersPanel()
    {
        var wordsAnswers = FillAnswers(_answersPanel, _allAnswers);
        _answersViews = wordsAnswers;

        foreach (var answer in _answersViews)
        {
            answer.OnSelected += ProcessAnswerSelect;
        }

    }
    protected override void FillRememberPanel()
    {
        FillAnswers(_rememberPanel, _rightAnswers);
    }

    private List<ImageAnswerView> FillAnswers(RectTransform parent, List<Texture2D> answersValues)
    {
        var wordsAnswers = new List<ImageAnswerView>();

        foreach (var answerValue in answersValues)
        {
            var answerView = Instantiate(_answerPrefab, parent);

            answerView.InitValue(answerValue);
            wordsAnswers.Add(answerView);
        }

        return wordsAnswers;
    }

    private void ProcessAnswerSelect(Texture2D value, ImageAnswerView imageAnswerView)
    {
        OnAnswerSelected?.Invoke(value, imageAnswerView);
    }

}
