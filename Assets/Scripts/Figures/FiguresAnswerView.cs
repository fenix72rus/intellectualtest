using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FiguresAnswerView : AnswerView<FigureGroup>
{
    public Action<FigureGroup, FiguresAnswerView> OnSelected;

    [SerializeField] private Image _figureImage, _symbolImage, _borderImage;
    [SerializeField] private RectTransform _selectedBorder;
    private RectTransform _rect;
    private Vector3 _startScale;

    public override void InitValue(FigureGroup value)
    {
        base.InitValue(value);

        _figureImage.sprite = value.Figure;
        _figureImage.color = value.Color;
        _symbolImage.sprite = value.Symbol;
        _borderImage.sprite = value.Border;

        ScaleSymbolBySprite();
    }

    private void ScaleSymbolBySprite()
    {
        var rect = _symbolImage.rectTransform;
        var texture = _symbolImage.sprite.texture;

        var width = rect.sizeDelta.x;
        var height = rect.sizeDelta.y;

        if (texture.width > texture.height)
        {
            height = width * ((float)texture.height / (float)texture.width);

        }
        else
        {
            width = height * ((float)texture.width / (float)texture.height);
        }

        rect.sizeDelta = new Vector2(width, height);
    }

    public override void SetSelectVisual()
    {
        _rect.localScale = _startScale * 0.85f;
        _selectedBorder.gameObject.SetActive(true);
    }
    public override void SetDeselectVisual()
    {
        _rect.localScale = _startScale;
        _selectedBorder.gameObject.SetActive(false);
    }

    protected override void ProcessAnswerClick()
    {
        OnSelected?.Invoke(_answerValue, this);
    }

    protected override void InitComponents()
    {
        base.InitComponents();

        _rect = this.GetComponent<RectTransform>();
        _startScale = _rect.localScale;
    }
}
