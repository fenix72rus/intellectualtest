using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FiguresGameView : GameModeView
{
    public Action<FigureGroup, FiguresAnswerView> OnAnswerSelected;

    [SerializeField] private FiguresAnswerView _answerPrefab;
    [SerializeField] private List<FiguresAnswerView> _answersViews = new List<FiguresAnswerView>();
    [SerializeField] private List<FigureGroup> _rightAnswers = new List<FigureGroup>(), _allAnswers = new List<FigureGroup>();

    public void Init(List<FigureGroup> rightAnswers, List<FigureGroup> allAnswers)
    {
        _answersViews.Clear();
        _allAnswers.Clear();
        _rightAnswers.Clear();

        _rightAnswers = rightAnswers;
        _allAnswers = allAnswers;
        Init();
    }

    public override void EnableAnswerInteracts(bool status)
    {
        foreach (var answer in _answersViews)
        {
            answer.EnableInteract(status);
        }
    }

    public override void ClearRememberPanel()
    {
        RemoveAllChildrens(_rememberPanel);
    }

    public override void ClearAnswersPanel()
    {
        RemoveAllChildrens(_answersPanel);
    }

    public override void EnableRightAnswersBorder()
    {
        foreach (var rightValue in _rightAnswers)
        {
            foreach (var answerView in _answersViews)
            {
                if (answerView.Value == rightValue)
                {
                    answerView.EnableRightBorder();
                    break;
                }
            }
        }
    }
    protected override void FillAnswersPanel()
    {
        var wordsAnswers = FillAnswers(_answersPanel, _allAnswers);
        _answersViews = wordsAnswers;

        foreach (var answer in _answersViews)
        {
            answer.OnSelected += ProcessAnswerSelect;
        }

    }
    protected override void FillRememberPanel()
    {
        FillAnswers(_rememberPanel, _rightAnswers);
    }
    private List<FiguresAnswerView> FillAnswers(RectTransform parent, List<FigureGroup> answersValues)
    {
        var wordsAnswers = new List<FiguresAnswerView>();

        foreach (var answerValue in answersValues)
        {
            var answerView = Instantiate(_answerPrefab, parent);

            answerView.InitValue(answerValue);
            wordsAnswers.Add(answerView);
        }

        return wordsAnswers;
    }

    private void ProcessAnswerSelect(FigureGroup value, FiguresAnswerView answerView)
    {
        OnAnswerSelected?.Invoke(value, answerView);
    }

}
