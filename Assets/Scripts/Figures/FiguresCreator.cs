using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiguresCreator : MonoBehaviour
{
    [SerializeField] private Sprite[] _figureSprites;
    [SerializeField] private Sprite[] _figureBorderSprites;
    [SerializeField] private Sprite[] _symbolSprites;
    [SerializeField] private Color[] _figureColors;
    [SerializeField] private Color _defaulColor;

    public List<FigureGroup> GetGroups(bool randomColor)
    {
        var color = _defaulColor;

        var groups = new List<FigureGroup>();

        for(int i = 0; i < _figureSprites.Length; i++)
        {
            var figureNumber = i;
            var figure = GetFigureByNumber(figureNumber);
            var border = GetBorderByNumber(figureNumber);

            for(int j = 0; j < _symbolSprites.Length; j++)
            {
                var symbolNumber = j;
                var symbol = GetSymbolByNumber(symbolNumber);

                if (randomColor)
                    color = _figureColors[Random.Range(0, _figureColors.Length)];
                var group = new FigureGroup(figureNumber, symbolNumber, figure, symbol, border, color);

                groups.Add(group);
            }
        }

        return groups;
    }

    private Sprite GetFigureByNumber(int number)
    {
        return _figureSprites[number];
    }

    private Sprite GetSymbolByNumber(int number)
    {
        return _symbolSprites[number];
    }

    private Sprite GetBorderByNumber(int number)
    {
        return _figureBorderSprites[number];
    }

    private void Awake()
    {
        _defaulColor = _figureColors[Random.Range(0, _figureColors.Length)];
    }
}

public class FigureGroup
{
    public int FigureNumber;
    public int SymbolNumber;
    public Sprite Figure;
    public Sprite Symbol;
    public Sprite Border;
    public Color Color;

    public FigureGroup(int figureNumber, int symbolNumber, Sprite figureSprite, Sprite symbolSprite, Sprite border, Color color)
    {
        FigureNumber = figureNumber;
        SymbolNumber = symbolNumber;
        Figure = figureSprite;
        Symbol = symbolSprite;
        Border = border;
        Color = color;
    }
}
