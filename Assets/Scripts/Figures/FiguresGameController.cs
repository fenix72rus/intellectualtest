using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiguresGameController : GameModeController<FigureGroup>
{
    private FiguresGameView _gameView;

    public FiguresGameController(GameMode<FigureGroup> gameMode, GameModeView gameModeView, float rememberDuration, float playStateDuration)
        : base(gameMode, gameModeView, rememberDuration, playStateDuration)
    {
        _gameView = gameModeView as FiguresGameView;
        _gameView.OnAnswerSelected += ProcessSelectAnswer;
    }

    public override void StartGame()
    {
        Debug.Log($"Right answers count {_gameMode.RightAnswers.Count}/All answers Count {_gameMode.AllAnswers.Count}");
        _gameView.Init(_gameMode.RightAnswers, _gameMode.AllAnswers);

        base.StartGame();
    }
}
