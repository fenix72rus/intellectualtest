using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public abstract class GameModeView : MonoBehaviour
{
    public Action OnGameStartPressed;
    public Action OnTimerEnded;
    public Action OnDoneButtonSelected;

    public float RemainingTimerTime =>_timer.RemainingTime;

    [SerializeField] private Button _doneButton;
    [SerializeField] protected RectTransform _rememberPanel, _answersPanel;
    [SerializeField] private TaskMessangeShower _messangeShower;
    [SerializeField] private Timer _timer;

    public abstract void EnableAnswerInteracts(bool status);
    public abstract void ClearRememberPanel();
    public abstract void ClearAnswersPanel();

    public void DestoyView()
    {
        Destroy(this.gameObject);
    }

    public void StartTimer(float duration)
    {
        _timer.StartTimer(duration);
    }

    public void StopTimer()
    {
        _timer.StopTimer();
    }

    public void ShowPrepareGameMessange(string messange)
    {
        _messangeShower.Show(messange);

        _messangeShower.OnPanelClosed += ProcessClosedMessangePanel;
    }

    public virtual void ShowRememberPanel()
    {
        _rememberPanel.gameObject.SetActive(true);
    }

    public virtual void HideRememberPanel()
    {
        _rememberPanel.gameObject.SetActive(false);
    }

    public virtual void ShowAnswersPanel()
    {
        _answersPanel.gameObject.SetActive(true);
    }


    public virtual void HideAnswersPanel()
    {
        _answersPanel.gameObject.SetActive(false);
    }

    public abstract void EnableRightAnswersBorder();

    protected void Init()
    {
        Debug.Log("View was inited", this.gameObject);
        FillRememberPanel();
        FillAnswersPanel();

        _timer.OnTimerEnded += () => OnTimerEnded?.Invoke();
        _doneButton.onClick.AddListener(() => OnDoneButtonSelected?.Invoke());
        _doneButton.onClick.AddListener(() => _doneButton.enabled = false);
    }

    private void ProcessClosedMessangePanel()
    {
        Debug.Log("Close Messange Panel");
        OnGameStartPressed?.Invoke();
        _messangeShower.Hide();
        _messangeShower.OnPanelClosed -= ProcessClosedMessangePanel;
    }

    protected void RemoveAllChildrens(RectTransform parent)
    {
        for (int i = parent.childCount - 1; i >= 0; i--)
        {
            var child = parent.GetChild(i);
            Destroy(child.gameObject);
        }
    }

    protected abstract void FillRememberPanel();
    protected abstract void FillAnswersPanel();

    private void OnEnable()
    {
        _answersPanel.gameObject.SetActive(false);
        _rememberPanel.gameObject.SetActive(false);
    }
}
