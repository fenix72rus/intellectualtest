using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameStarterView : MonoBehaviour
{
    public Action OnGameStarted;

    [SerializeField] private Button _startGameButton;
    [SerializeField] private RectTransform _startGamePanel, _gamePanel;
    [SerializeField] private TimeSetterController _timeSetterController;

    private void Start()
    {
        _timeSetterController.OnTimeValuesSetted += () => _startGamePanel.gameObject.SetActive(true);
        _startGameButton.onClick.AddListener(StartGame);
    }

    private void StartGame()
    {
        OnGameStarted?.Invoke();

        _gamePanel.gameObject.SetActive(true);
        Destroy(this.gameObject);
    }
}
