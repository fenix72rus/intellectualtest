using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModeData<T>
{
    public List<T> GameDatas => _datas; 

    private List<T> _datas;

    public GameModeData(List<T> dataList)
    {
        _datas = dataList;
    }
}
