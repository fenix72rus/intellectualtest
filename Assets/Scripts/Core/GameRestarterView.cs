using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameRestarterView : MonoBehaviour
{
    [SerializeField] private Button _restartButton;

    public void EnableRestartGame()
    {
        _restartButton.gameObject.SetActive(true);
    }

    private void Start()
    {
        _restartButton.gameObject.SetActive(false);
        _restartButton.onClick.AddListener(ProcessButtonClick);
    }

    private void ProcessButtonClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
