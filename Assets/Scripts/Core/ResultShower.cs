using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ResultShower : MonoBehaviour
{
    [SerializeField] private TMP_Text _totalScoreText, _wordsScoreText, _imagesScoreText, _figuresScoreText, _audiosScoreText;

    [SerializeField] private string _singleWord, _duoWord, _otherWord;

    public void ShowResult(Result result)
    {
        WriteResult(result.WordsScore, _wordsScoreText, "�����");
        WriteResult(result.ImagesScore, _imagesScoreText, "�����������");
        WriteResult(result.FiguresScore, _figuresScoreText, "������");
        WriteResult(result.AudiosScore, _audiosScoreText, "�����");

        _totalScoreText.text = $"����� ���� - {result.TotalScore} {GetWordVariantByScore(result.TotalScore)}";
    }

    private void WriteResult(ResultData result, TMP_Text text, string modeName)
    {
        if (result == null)
            return;

        text.text = $"{modeName} - {result.RightAnswersCount} {GetWordVariantByScore(result.RightAnswersCount)}";
    }

    private string GetWordVariantByScore(int score)
    {
        if(score % 10 == 1)
        {
            return _singleWord;
        }

        if(score % 10 == 2 || score % 10 == 3 || score % 10 == 4)
        {
            return _duoWord;
        }

        return _otherWord;
    }
}
