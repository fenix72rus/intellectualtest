using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameMode<T>
{
    public Action<List<T>> OnRightAnswersSelected, OnAnswersListSelected;

    public List<T> RightAnswers => _rightAnswers;
    public List<T> AllAnswers => _answers;

    public string MessageBeforeStart => _messageBeforeStart;
    public int RightAnswersCount => _rightAnswersCount;

    protected GameModeData<T> _gameModeData;

    private int _rightAnswersCount = 1;
    private int _answersCount = 1;
    private string _messageBeforeStart;
    private List<T> _answers, _rightAnswers;

    public GameMode(GameModeData<T> gameModeData, int countOfAnswers, int countOfRightAnswers, string messageBeforeStart)
    {
        _gameModeData = gameModeData;

        _answersCount = Mathf.Clamp(countOfAnswers, 1, _gameModeData.GameDatas.Count);
        _rightAnswersCount = Mathf.Clamp(countOfRightAnswers, 1, _answersCount);
        _messageBeforeStart = messageBeforeStart;
    }

    public virtual void InitGame()
    {
        if (_gameModeData == null)
            Debug.LogError($"GameModeData is not initialized");

        SelectAnswers();
        SelectRightAnswers();
    }

    private void SelectAnswers()
    {
        if (_gameModeData.GameDatas.Count < _answersCount)
        {
            throw new Exception("GameData contains less than the requested count");
        }

        _answers = FillRandomVariantsFromList(_gameModeData.GameDatas, _answersCount);

        OnAnswersListSelected?.Invoke(_answers);
    }

    private void SelectRightAnswers()
    {
        if(_answers == null || _answers.Count == 0)
        {
            throw new Exception("Answers is null");
        }

        if(_rightAnswersCount > _answers.Count)
        {
            throw new Exception("All answers count less than the right answers count");
        }

        _rightAnswers = FillRandomVariantsFromList(_answers, _rightAnswersCount);

        OnRightAnswersSelected?.Invoke(_rightAnswers);
    }

    private List<T> FillRandomVariantsFromList(List<T> sourceList, int variantsCount)
    {
        variantsCount = Mathf.Max(1, variantsCount);

        var randomList = new List<T>();

        for (int i = 0; i < variantsCount; i++)
        {
            T randomData;

            do
            {
                randomData = GetRandomData(sourceList);
            }
            while (randomList.Contains(randomData));

            randomList.Add(randomData);
        }

        return randomList;
    }

    private T GetRandomData(List<T> dataList)
    {
        var randomIndex = UnityEngine.Random.Range(0, dataList.Count);

        var data = dataList[randomIndex];

        return data;
    }
}
