using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class GameModeController <T>
{
    public Action<ResultData> OnGameEnded;

    protected int AnswersMadeCount => _selectedAnswers.Count;

    protected GameMode<T> _gameMode;
    protected GameModeView _gameModeView;
    protected int _playerScore;
    protected List<T> _selectedAnswers = new List<T>();

    private int _rememberDurationInMilliseconds;
    private int _playDurationInMilliseconds;
    private bool _endGameInProgress;

    public GameModeController(GameMode<T> gameMode, GameModeView gameModeView, float rememberDuration, float playStateDuration)
    {
        InitGameMode(gameMode);
        _gameModeView = gameModeView;

        _rememberDurationInMilliseconds = (int)(rememberDuration * 1000);
        _playDurationInMilliseconds = (int)(playStateDuration * 1000);
        _playerScore = 0;
    }

    public virtual void StartGame()
    {
        Debug.Log($"<color=yellow>Start remember state of {typeof(T)} {this.GetType()} </color>");
        InitGameView(_gameModeView);
        _gameModeView.ShowPrepareGameMessange(_gameMode.MessageBeforeStart);
        _gameModeView.OnGameStartPressed += StartRememberState;
    }

    protected virtual void StartRememberState()
    {
        _gameModeView.ShowRememberPanel();
        _gameModeView.EnableAnswerInteracts(false);

        RememberDelay();
    }

    protected void ProcessRightAnswer()
    {
        _playerScore++;
    }

    protected void ProcessRemoveRightAnswer()
    {
        _playerScore--;
    }

    protected async void EndGame()
    {
        if (_endGameInProgress)
            return;

        _endGameInProgress = true;
        _gameModeView.StopTimer();
        _gameModeView.EnableAnswerInteracts(false);
        VisualIdentifyCorrentAnswers();

        await System.Threading.Tasks.Task.Delay(3000);

        ClearGameView();

        Debug.Log($"<color=green>Controller {typeof(T)} was ended</color>");

        var countOfWrongSelected = _selectedAnswers.Count - _playerScore;
        var totalWrongsAnswersCount = _gameMode.AllAnswers.Count - _gameMode.RightAnswersCount;
        var completionTime = (float)_playDurationInMilliseconds / 1000f - _gameModeView.RemainingTimerTime;
        Debug.Log($"<color=red>Completion time - {completionTime}</color>");
        var resultData = 
            new ResultData(_playerScore, _gameMode.RightAnswersCount, countOfWrongSelected, totalWrongsAnswersCount, completionTime);

        OnGameEnded?.Invoke(resultData);
        _gameModeView.DestoyView();
    }

    protected void VisualIdentifyCorrentAnswers()
    {
        _gameModeView.EnableRightAnswersBorder();
    }

    private void ClearGameView()
    {

        _gameModeView.ClearAnswersPanel();
        _gameModeView.ClearRememberPanel();

        _gameModeView.HideAnswersPanel();
        _gameModeView.HideRememberPanel();
    }

    private async void RememberDelay()
    {
        await System.Threading.Tasks.Task.Delay(_rememberDurationInMilliseconds);

        _gameModeView.HideRememberPanel();

        StartGameState();
    }

    private void StartGameState()
    {
        _gameModeView.ShowAnswersPanel();
        _gameModeView.EnableAnswerInteracts(true);
        _gameModeView.StartTimer(_playDurationInMilliseconds / 1000);
    }

    private void InitGameMode(GameMode<T> gameMode)
    {
        _gameMode = gameMode;
        _gameMode.InitGame();
    }

    private void InitGameView(GameModeView gameModeView)
    {
        _gameModeView.OnGameStartPressed += StartRememberState;
        _gameModeView.OnDoneButtonSelected += EndGame;
        _gameModeView.OnTimerEnded += EndGame;
    }

    protected virtual void ProcessSelectAnswer(T value, AnswerView<T> answerView)
    {
        Debug.Log($"answer selected {value}. {AnswersMadeCount} {_playerScore} {_gameMode.RightAnswers.Count}");


        if(_selectedAnswers.Contains(value))
        {
            RemoveAnswer(value, answerView);
        }
        else
        {
            AddAnswer(value, answerView);
        }
    }

    private void RemoveAnswer(T value, AnswerView<T> answerView)
    {
        _selectedAnswers.Remove(value);
        answerView.SetDeselectVisual();

        if (_gameMode.RightAnswers.Contains(value))
        {
            ProcessRemoveRightAnswer();
        }
    }

    private void AddAnswer(T value, AnswerView<T> answerView)
    {
        if (AnswersMadeCount >= _gameMode.RightAnswers.Count)
        {
            return;
        }

        _selectedAnswers.Add(value);
        answerView.SetSelectVisual();

        if (_gameMode.RightAnswers.Contains(value))
        {
            ProcessRightAnswer();
        }
    }
}
