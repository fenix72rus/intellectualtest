using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public abstract class AnswerView<T> : MonoBehaviour
{
    public T Value => _answerValue;

    [SerializeField] protected Button _button;
    [SerializeField] protected T _answerValue;
    [SerializeField] private RectTransform _rightBorder;

    public virtual void InitValue(T value)
    {
        _answerValue = value;
    }

    public void EnableInteract(bool status)
    {
        if(_button)
            _button.enabled = status;
    }

    public void EnableRightBorder()
    {
        if(_rightBorder)
            _rightBorder.gameObject.SetActive(true);
    }

    public abstract void SetSelectVisual();
    public abstract void SetDeselectVisual();

    private void Start()
    {
        InitComponents();
    }

    protected virtual void InitComponents()
    {
        _button.onClick.AddListener(ProcessAnswerClick);
    }

    protected abstract void ProcessAnswerClick();

}
