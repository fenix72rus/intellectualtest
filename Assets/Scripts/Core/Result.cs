using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Result
{
    public ResultData WordsScore;
    public ResultData ImagesScore;
    public ResultData FiguresScore;
    public ResultData AudiosScore;
    public int TotalScore
    {
        get
        {
            var score = 
                WordsScore.RightAnswersCount + ImagesScore.RightAnswersCount 
                + FiguresScore.RightAnswersCount + AudiosScore.RightAnswersCount;

            return score;
        }
    }
}

[System.Serializable]
public class ResultData
{
    public int RightAnswersCount;
    public int TotalRightAnswersCount;
    public int WrongAnswersCount;
    public int TotalWrongAnswersCount;
    public float TaskCompletionTime;

    public ResultData(int rightAnswersCount, int totalRightAnswersCount, int wrongAnswersCount, int totalWrongAnswersCount, float taskCompletionTime)
    {
        RightAnswersCount = rightAnswersCount;
        TotalRightAnswersCount = totalRightAnswersCount;
        WrongAnswersCount = wrongAnswersCount;
        TotalWrongAnswersCount = totalWrongAnswersCount;
        TaskCompletionTime = taskCompletionTime;
    }

    public static ResultData operator +(ResultData r1, ResultData r2)
    {
        return new ResultData(r1.RightAnswersCount + r2.RightAnswersCount, r1.TotalRightAnswersCount + r2.TotalRightAnswersCount,
            r1.WrongAnswersCount + r2.WrongAnswersCount, r1.TotalWrongAnswersCount + r2.TotalWrongAnswersCount,
            r1.TaskCompletionTime + r2.TaskCompletionTime);
    }
}
