using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum PlayMode
{ 
    Words,
    Images,
    Figures,
    Audios
}

public class GameManager : MonoBehaviour
{
    public Action<Result> OnGameEnded;

    [SerializeField] private GameModeCreator _gameModeCreator;
    [SerializeField] private List<PlayMode> _playModes;
    [SerializeField] private GameStarterView _gameStarterView;
    [SerializeField] private ResultShower _resultShower;
    [SerializeField] private GameRestarterView _gameRestarterView;

    private Result _result;
    private ResultSaver _resultSaver;
    private Queue<WordsGameController> _wordsGameControllers = new Queue<WordsGameController>();
    private Queue<ImagesGameController> _imagesGameControllers = new Queue<ImagesGameController>();
    private Queue<FiguresGameController> _figuresGameControllers = new Queue<FiguresGameController>();
    private Queue<AudioGameController> _audioGameControllers = new Queue<AudioGameController>();
    private PlayMode _currentPlayMode;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        _gameStarterView.OnGameStarted += StartGame;

        _result = new Result();
        _resultSaver = new ResultSaver("Results");

        OnGameEnded += _resultShower.ShowResult;
        OnGameEnded += _resultSaver.Save;
        OnGameEnded += (Result result) => _gameRestarterView.EnableRestartGame();
    }

    private void InitGameModes()
    {
        var lightWords = _gameModeCreator.GetLightWordsMode();
        InitWordsMode(lightWords);

        var hardWords = _gameModeCreator.GetHardWordsMode();
        InitWordsMode(hardWords);

        var lightImages = _gameModeCreator.GetLightImagesMode();
        InitImagesMode(lightImages);

        var mediumImages = _gameModeCreator.GetMediumImagesMode();
        InitImagesMode(mediumImages);

        var hardImages = _gameModeCreator.GetHardImagesMode();
        InitImagesMode(hardImages);

        var lightFigure = _gameModeCreator.GetLightFigureMode();
        InitFiguresMode(lightFigure);

        var hardFigure = _gameModeCreator.GetHardFigureMode();
        InitFiguresMode(hardFigure);

        var lightAudio = _gameModeCreator.GetLightAudioMode();
        InitAudioMode(lightAudio);

        var mediumAudio = _gameModeCreator.GetMediumAudioMode();
        InitAudioMode(mediumAudio);

        var hardAudio = _gameModeCreator.GetHardAudioMode();
        InitAudioMode(hardAudio);
    }

    private void InitAudioMode(AudioGameController gameController)
    {
        gameController.OnGameEnded += AddAudioScore;
        gameController.OnGameEnded += ProcessEndGameMode;

        _audioGameControllers.Enqueue(gameController);
    }


    private void StartGame()
    {
        InitGameModes();

        _currentPlayMode = _playModes[0];
        PlayController();
    }

    private void InitWordsMode(WordsGameController wordsGameController)
    {
        wordsGameController.OnGameEnded += AddWordsScore;
        wordsGameController.OnGameEnded += ProcessEndGameMode;

        _wordsGameControllers.Enqueue(wordsGameController);
    }

    private void InitImagesMode(ImagesGameController imagesGameController)
    {
        imagesGameController.OnGameEnded += AddImagesScore;
        imagesGameController.OnGameEnded += ProcessEndGameMode;

        _imagesGameControllers.Enqueue(imagesGameController);
    }

    private void InitFiguresMode(FiguresGameController figuresGameController)
    {
        figuresGameController.OnGameEnded += AddFigureScore;
        figuresGameController.OnGameEnded += ProcessEndGameMode;

        _figuresGameControllers.Enqueue(figuresGameController);
    }

    private void ProcessEndGameMode(ResultData result)
    {
        PlayController();
    }

    private void AddWordsScore(ResultData result)
    {
        Debug.Log($"Game was ended. Player score is {result.RightAnswersCount}");
        if(_result.WordsScore == null)
        {
            _result.WordsScore = result;
        }
        else
        {
            _result.WordsScore += result;
        }
    }

    private void AddImagesScore(ResultData result)
    {
        Debug.Log($"Game was ended. Player score is {result.RightAnswersCount}");
        if (_result.ImagesScore == null)
        {
            _result.ImagesScore = result;
        }
        else
        {
            _result.ImagesScore += result;
        }
    }

    private void AddFigureScore(ResultData result)
    {
        Debug.Log($"Game was ended. Player score is {result.RightAnswersCount}");

        if (_result.FiguresScore == null)
        {
            _result.FiguresScore = result;
        }
        else
        {
            _result.FiguresScore += result;
        }
    }

    private void AddAudioScore(ResultData result)
    {
        Debug.Log($"Game was ended. Player score is {result.RightAnswersCount}");

        if (_result.AudiosScore == null)
        {
            _result.AudiosScore = result;
        }
        else
        {
            _result.AudiosScore += result;
        }
    }

    private void PlayController()
    {
        switch(_currentPlayMode)
        {
            case PlayMode.Words:
                PlayWords();
                break;
            case PlayMode.Images:
                PlayImages();
                break;
            case PlayMode.Figures:
                PlayFigures();
                break;
            case PlayMode.Audios:
                PlayAudios();
                break;
        }
    }

    private void PlayAudios()
    {
        if (_audioGameControllers.Count == 0)
        {
            SwitchToNextMode();
        }
        else
        {
            var controller = _audioGameControllers.Dequeue();
            controller.StartGame();
        }
    }

    private void PlayWords()
    {
        if(_wordsGameControllers.Count == 0)
        {
            SwitchToNextMode();
        }
        else
        {
            var words = _wordsGameControllers.Dequeue();
            words.StartGame();
        }
    }

    private void PlayImages()
    {
        if (_imagesGameControllers.Count == 0)
        {
            SwitchToNextMode();
        }
        else
        {
            var images = _imagesGameControllers.Dequeue();
            images.StartGame();
        }
    }

    private void PlayFigures()
    {
        if (_figuresGameControllers.Count == 0)
        {
            SwitchToNextMode();
        }
        else
        {
            var controller = _figuresGameControllers.Dequeue();
            controller.StartGame();
        }
    }

    private void SwitchToNextMode()
    {
        var newModeNumber = _playModes.IndexOf(_currentPlayMode) + 1;

        if(newModeNumber >= _playModes.Count)
        {
            Debug.Log("End game");
            OnGameEnded?.Invoke(_result);
        }
        else
        {
            Debug.Log("Go to next mode");
            _currentPlayMode = _playModes[newModeNumber];
            PlayController();
        }
    }
}
