using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Newtonsoft.Json;

public class ResultSaver : WriteReadUtility
{
    private string _savePath;

    public ResultSaver(string saveFolderName)
    {
        _savePath = Path.Combine(Application.dataPath, saveFolderName);

        if(!Directory.Exists(_savePath))
        {
            Directory.CreateDirectory(_savePath);
        }
    }

    public void Save(Result result)
    {
        var fileName = DateTime.UtcNow.ToString("HH-mm dd MMMM yyyy") + ".json";
        var path = Path.Combine(_savePath, fileName);

        var jsonData = JsonConvert.SerializeObject(result, Formatting.Indented);

        WriteFile(path, jsonData);
    }
}
