using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModeCreator : MonoBehaviour
{
    [SerializeField] private GameModeSettings _wordsLightModeSettings;
    [SerializeField] private GameModeSettings _wordsHardModeSettings;
    [SerializeField] private GameModeSettings _imagesLightSettings;
    [SerializeField] private GameModeSettings _imagesMediumSettings;
    [SerializeField] private GameModeSettings _imagesHardSettings;
    [SerializeField] private GameModeSettings _figureLightSettings;
    [SerializeField] private GameModeSettings _figureHardSettings;
    [SerializeField] private GameModeSettings _audioLightSettings;
    [SerializeField] private GameModeSettings _audioMediumSettings;
    [SerializeField] private GameModeSettings _audioHardSettings;

    [SerializeField] private TimeSetterController _timeSetterController;
    [SerializeField] private string[] _imagesGroupsPaths;
    [SerializeField] private FiguresCreator _figureCreator;
    [SerializeField] private AudioDataCreator _audioCreator;
    [SerializeField] private AudioSource _audioModeSource;
    [SerializeField] private AudioClip _audioModeStartClip;

    public WordsGameController GetLightWordsMode()
    {
        return GetWordsMode(_wordsLightModeSettings, "LightWordsMode.txt");
    }

    public WordsGameController GetHardWordsMode()
    {
        return GetWordsMode(_wordsHardModeSettings, "HardWordsMode.txt");
    }

    private WordsGameController GetWordsMode(GameModeSettings settings, string worldFileName)
    {
        var wordsReader = new WordsReader(settings.AssetsFolderName, worldFileName);

        var stringList = wordsReader.GetWords();

        var modeData = new GameModeData<string>(stringList);
        var wordsGameMode = new GameMode<string>(modeData, settings.AllAnswersCount, settings.RightAnswersCount, settings.PrepareGameMessange);
        var wordsController = new WordsGameController(wordsGameMode, settings.GameView, settings.RememberDurationInSeconds, settings.ResponseTimeInSeconds);

        return wordsController;
    }

    public ImagesGameController GetLightImagesMode()
    {
        return GetImagesMode(_imagesLightSettings);
    }

    public ImagesGameController GetMediumImagesMode()
    {
        return GetImagesMode(_imagesMediumSettings);
    }

    public ImagesGameController GetHardImagesMode()
    {
        var randomGroup = _imagesGroupsPaths[Random.Range(0, _imagesGroupsPaths.Length)];

        return GetImagesMode(_imagesHardSettings, randomGroup);
    }

    private ImagesGameController GetImagesMode(GameModeSettings settings, string groupPath = "")
    {
        ImagesReader imagesReader;
        if (groupPath == "")
        {
            imagesReader = new ImagesReader(settings.AssetsFolderName);
        }
        else
        {
            imagesReader = new ImagesReader(groupPath);
        }

        var imagesList = imagesReader.GetImages();

        var modeData = new GameModeData<Texture2D>(imagesList);
        var gameMode = new GameMode<Texture2D>(modeData, settings.AllAnswersCount, settings.RightAnswersCount, settings.PrepareGameMessange);
        var controller = new ImagesGameController(gameMode, settings.GameView, settings.RememberDurationInSeconds, settings.ResponseTimeInSeconds);

        return controller;
    }

    public FiguresGameController GetLightFigureMode()
    {
        var figureGroups = _figureCreator.GetGroups(false);
        var controller = GetFigureController(_figureLightSettings, figureGroups);

        return controller;
    }

    public FiguresGameController GetHardFigureMode()
    {
        var figureGroups = _figureCreator.GetGroups(true);
        var controller = GetFigureController(_figureHardSettings, figureGroups);

        return controller;
    }

    private FiguresGameController GetFigureController(GameModeSettings settings, List<FigureGroup> figureGroups)
    {
        var modeData = new GameModeData<FigureGroup>(figureGroups);
        var gameMode = new GameMode<FigureGroup>(modeData, settings.AllAnswersCount, settings.RightAnswersCount, settings.PrepareGameMessange);
        var controller = new FiguresGameController(gameMode, settings.GameView, settings.RememberDurationInSeconds, settings.ResponseTimeInSeconds);

        return controller;
    }

    public AudioGameController GetLightAudioMode()
    {
        var data = _audioCreator.GetRandomData(_audioLightSettings.AllAnswersCount);

        return GetAudioController(_audioLightSettings, data);
    }

    public AudioGameController GetMediumAudioMode()
    {
        var data = _audioCreator.GetRandomData(_audioMediumSettings.AllAnswersCount);

        return GetAudioController(_audioMediumSettings, data);
    }

    public AudioGameController GetHardAudioMode()
    {
        var data = _audioCreator.GetRandomData(_audioHardSettings.AllAnswersCount);

        return GetAudioController(_audioHardSettings, data);
    }

    private AudioGameController GetAudioController(GameModeSettings settings, AudioTaskData data)
    {
        var datas = new List<AudioTaskData> { data };
        var modeData = new GameModeData<AudioTaskData>(datas);
        var gameMode = new GameMode<AudioTaskData>(modeData, settings.AllAnswersCount, settings.RightAnswersCount, settings.PrepareGameMessange);
        var controller = new AudioGameController(gameMode, settings.GameView, _audioModeSource, _audioModeStartClip, settings.ResponseTimeInSeconds);

        return controller;
    }

    private void Start()
    {
        var settings = new List<GameModeSettings>();

        settings.Add(_wordsLightModeSettings);
        settings.Add(_wordsHardModeSettings);
        settings.Add(_imagesLightSettings);
        settings.Add(_imagesHardSettings);
        settings.Add(_figureLightSettings);
        settings.Add(_figureHardSettings);
        settings.Add(_audioLightSettings);
        settings.Add(_audioHardSettings);

        _timeSetterController.InitSettings(settings);
    }
}

[System.Serializable]
public class GameModeSettings
{
    public int AllAnswersCount;
    public int RightAnswersCount;
    public GameModeView GameView;
    public float RememberDurationInSeconds;
    public float ResponseTimeInSeconds;
    public string AssetsFolderName;
    [TextArea]
    public string PrepareGameMessange;
}
