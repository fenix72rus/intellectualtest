using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace ScreenKeyboard
{
    public class KeyButton<T> : MonoBehaviour
    {
        public Action<T> OnKeyPressed;
        public T KeyValue => _keyValue;

        [SerializeField] private T _keyValue;
        [SerializeField] private Button _button;

        private void Awake()
        {
            _button.onClick.AddListener(() => OnKeyPressed?.Invoke(KeyValue));
        }
    }
}