using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace ScreenKeyboard
{
    public class Keyboard : MonoBehaviour
    {
        public Action<int> OnNumberKeyPressed;
        public Action OnBackspacePressed;

        [SerializeField] private NumberButton[] _numbersButtons;
        [SerializeField] private Button _backspaceButton;

        private void Awake()
        {
            Init();
        }

        private void Init()
        {
            foreach(var button in _numbersButtons)
            {
                button.OnKeyPressed += (int value) => this.OnNumberKeyPressed?.Invoke(value);
            }

            _backspaceButton.onClick.AddListener(() => OnBackspacePressed?.Invoke());
        }
    }
}