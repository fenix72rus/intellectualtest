using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class WordsReader 
{
    private string _wordsFilePath;

    public WordsReader(string assetsFolder, string wordsFileName)
    {
        _wordsFilePath = Path.Combine(Application.streamingAssetsPath, assetsFolder, wordsFileName);

    }

    public List<string> GetWords()
    {
        if (!File.Exists(_wordsFilePath))
            throw new System.Exception($"Has no file {_wordsFilePath}");

        var words = new List<string>();

        var streamReader = new StreamReader(_wordsFilePath);

        string line = streamReader.ReadLine();

        while (line != null)
        {
            words.Add(line); 
            line = streamReader.ReadLine();
        }
        streamReader.Close();

        return words;
    }
}
