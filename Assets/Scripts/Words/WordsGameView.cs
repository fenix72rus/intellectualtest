using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WordsGameView : GameModeView
{
    public Action<string, WordAnswerView> OnAnswerSelected;

    [SerializeField] private WordAnswerView _answerPrefab;
    [SerializeField] private List<WordAnswerView> _answersViews = new List<WordAnswerView>();
    [SerializeField] private List<string> _rightAnswers, _allAnswers;

    public void Init(List<string> rightAnswers, List<string> allAnswers)
    {
        _answersViews.Clear();
        _allAnswers.Clear();
        _rightAnswers.Clear();

        _rightAnswers = rightAnswers;
        _allAnswers = allAnswers;
        Init();
    }

    public override void EnableAnswerInteracts(bool status)
    {
        foreach (var answer in _answersViews)
        {
            answer.EnableInteract(status);
        }
    }

    public override void ClearRememberPanel()
    {
        RemoveAllChildrens(_rememberPanel);
    }

    public override void ClearAnswersPanel()
    {
        RemoveAllChildrens(_answersPanel);
    }

    public override void EnableRightAnswersBorder()
    {
        foreach(var rightValue in _rightAnswers)
        {
            foreach(var answerView in _answersViews)
            {
                if(answerView.Value == rightValue)
                {
                    answerView.EnableRightBorder();
                    break;
                }
            }
        }
    }

    protected override void FillAnswersPanel()
    {
        var wordsAnswers = FillAnswers(_answersPanel, _allAnswers);
        _answersViews = wordsAnswers;

        foreach(var answer in _answersViews)
        {
            answer.OnSelected += ProcessAnswerSelect;
        }

    }

    protected override void FillRememberPanel()
    {
        FillAnswers(_rememberPanel, _rightAnswers);
    }

    private List<WordAnswerView> FillAnswers(RectTransform parent, List<string> answersValues)
    {
        var wordsAnswers = new List<WordAnswerView>();

        foreach (var answerValue in answersValues)
        {
            var answerView = Instantiate(_answerPrefab, parent);

            answerView.InitValue(answerValue);
            wordsAnswers.Add(answerView);
        }

        return wordsAnswers;
    }

    private void ProcessAnswerSelect(string value, WordAnswerView wordAnswerView)
    {
        OnAnswerSelected?.Invoke(value, wordAnswerView);
    }
}
