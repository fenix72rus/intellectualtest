using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class WordAnswerView : AnswerView<string>
{
    public Action<string, WordAnswerView> OnSelected;

    [SerializeField] private TMP_Text _text;

    public override void InitValue(string value)
    {
        base.InitValue(value);
        _text.text = value;
    }

    public override void SetDeselectVisual()
    {
        _text.color = Color.white;
    }

    public override void SetSelectVisual()
    {
        _text.color = Color.gray;
    }

    protected override void ProcessAnswerClick()
    {
        OnSelected?.Invoke(_answerValue, this);
    }
}
