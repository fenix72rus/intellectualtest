using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordsGameController : GameModeController<string>
{
    private WordsGameView _wordsGameView;

    public WordsGameController(GameMode<string> gameMode, GameModeView gameModeView, float rememberDuration, float playStateDuration) 
        : base (gameMode, gameModeView, rememberDuration, playStateDuration)
    {
        _wordsGameView = gameModeView as WordsGameView;
        _wordsGameView.OnAnswerSelected += ProcessSelectAnswer;

        gameMode.OnAnswersListSelected += (List<string> list) => DebugList(list, "Answer variant");
        gameMode.OnRightAnswersSelected += (List<string> list) => DebugList(list, "Right answer");
    }

    public override void StartGame()
    {
        Debug.Log($"Right answers count {_gameMode.RightAnswers.Count}/All answers Count {_gameMode.AllAnswers.Count}");
        _wordsGameView.Init(_gameMode.RightAnswers, _gameMode.AllAnswers);

        base.StartGame();
    }

    private void DebugList<T>(List<T> list, string additional)
    {
        string field = additional + " ";

        foreach (var element in list)
        {
            field += element;
            field += " ";
        }

        Debug.Log(field);
    }
}
