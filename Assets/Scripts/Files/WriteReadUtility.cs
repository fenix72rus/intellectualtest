using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public abstract class WriteReadUtility
{
    protected void WriteFile(string path, string content)
    {
        //FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate);

        using (StreamWriter writer = new StreamWriter(path, false))
        {
            writer.Write(content);
            writer.Close();
        }

        //fileStream.Close();
    }

    protected string ReadFile(string path)
    {
        var content = "";
        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                content = reader.ReadToEnd();
                reader.Close();
            }
        }

        return content;
    }

    protected void CreateDirectoryIfNotExist(string path)
    {
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
    }
}
