using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class TaskMessangeShower : MonoBehaviour
{
    public Action OnPanelClosed;

    [SerializeField] private RectTransform _messangePanel;
    [SerializeField] private TMP_Text _prepareGameMessangeText;
    [SerializeField] private Button _closePanelButton;

    public void Show(string messange)
    {
        _closePanelButton.enabled = true;
        _messangePanel.gameObject.SetActive(true);
        _prepareGameMessangeText.text = messange;
    }

    public void Hide()
    {
        _messangePanel.gameObject.SetActive(false);
        _prepareGameMessangeText.text = "";
    }

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        _closePanelButton.onClick.AddListener(ProcessClosePanel);
    }

    private void ProcessClosePanel()
    {
        OnPanelClosed?.Invoke();
        _closePanelButton.enabled = false;
    }
}
