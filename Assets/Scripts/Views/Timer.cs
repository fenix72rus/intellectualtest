using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class Timer : MonoBehaviour
{
    public Action OnTimerEnded;

    public float RemainingTime
    {
        get
        {
            return Mathf.Max(0, _remainingTime);

        }
    }

    [SerializeField] private TMP_Text _timerText;

    private float _remainingTime;
    private IEnumerator _timerRutine;

    public void StartTimer(float durationInSeconds)
    {
        _timerText.enabled = true;

        if (_timerRutine != null)
            StopCoroutine(_timerRutine);

        _timerRutine = TimerRutine(durationInSeconds);

        StartCoroutine(_timerRutine);
    }

    public void StopTimer()
    {
        _timerText.enabled = false;

        if (_timerRutine != null)
            StopCoroutine(_timerRutine);
    }

    private IEnumerator TimerRutine(float duration)
    {
        _remainingTime = duration;
        while (_remainingTime > 0)
        {
            UpdateTimerView(_remainingTime);

            yield return new WaitForSeconds(1f);

            _remainingTime--;
        }

        OnTimerEnded?.Invoke();
        StopTimer();
    }

    private void UpdateTimerView(float remainingTime)
    {
        var minutes = (int)remainingTime / 60;
        var seconds = remainingTime % 60;

        _timerText.text = $"{minutes.ToString("00")}:{seconds.ToString("00")}";
    }
}
