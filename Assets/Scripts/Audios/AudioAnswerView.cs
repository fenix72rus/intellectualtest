using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using ScreenKeyboard;

public class AudioAnswerView : AnswerView<AudioTaskData>
{
    public Action<string> OnSelected;

    [SerializeField] private TMP_InputField _inputField;
    [SerializeField] private Keyboard _keyboard;

    public override void SetDeselectVisual()
    {
    }

    public override void SetSelectVisual()
    {

    }

    protected override void ProcessAnswerClick()
    {
        OnSelected?.Invoke(_inputField.text);
    }

    private void Awake()
    {
        InitKeyboard();
    }

    private void InitKeyboard()
    {
        _keyboard.OnNumberKeyPressed += ProcessNumberKeyPressed;
        _keyboard.OnBackspacePressed += ProcessBackspacePressed;
    }

    private void ProcessBackspacePressed()
    {
        var eneterdText = _inputField.text;

        if (eneterdText.Length <= 0)
            return;

        var chars = new char[eneterdText.Length];

        for (int i = 0; i < eneterdText.Length - 1; i++)
        {
            chars[i] = eneterdText[i];
        }

        var newText = new string(chars);


        _inputField.text = newText;
    }

    private void ProcessNumberKeyPressed(int value)
    {
        _inputField.text = _inputField.text + value.ToString();
    }
}
