using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioDataCreator : MonoBehaviour
{
    [SerializeField] private AudioClip[] _numbersAudioClips;

    public AudioTaskData GetRandomData(int numbersCount, int minElementValue = 0, int maxElementValue = 9)
    {
        numbersCount = Mathf.Clamp(numbersCount, 1, maxElementValue);
        maxElementValue = Mathf.Min(maxElementValue, _numbersAudioClips.Length - 1);
        minElementValue = Mathf.Max(0, minElementValue);

        if (minElementValue > maxElementValue)
            throw new System.Exception("The minimun number must be greater than tha maximum number");

        var audioTaskData = new AudioTaskData();

        for(int i = 0; i < numbersCount; i++)
        {
            int value;

            do
            {
                value = Random.Range(minElementValue, maxElementValue + 1);
            }
            while (audioTaskData.Elements.ContainsKey(value));

            var clip = _numbersAudioClips[value];

            audioTaskData.Elements.Add(value, clip);
        }

        return audioTaskData;
    }
}
