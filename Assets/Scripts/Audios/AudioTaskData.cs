using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTaskData
{
    public Dictionary<int, AudioClip> Elements = new Dictionary<int, AudioClip>();
}
