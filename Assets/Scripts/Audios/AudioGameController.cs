using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioGameController : GameModeController<AudioTaskData>
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _startAudioClip;

    private AudioGameView _gameView;

    public AudioGameController(GameMode<AudioTaskData> gameMode, 
        GameModeView gameModeView, 
        AudioSource audioSource, 
        AudioClip startClip, 
        float playStateDuration)
        : base(gameMode, gameModeView, startClip.length * 5f, playStateDuration)
    {
        _gameView = gameModeView as AudioGameView;
        _gameView.Init(gameMode.AllAnswers[0]);

        _audioSource = audioSource;
        _startAudioClip = startClip;

        _gameView.OnAnswerSelected += ProcessSelectAnswer;
    }

    protected override void StartRememberState()
    {
        base.StartRememberState();
        PlayClips();
    }

    public override void StartGame()
    {
        base.StartGame();

        Debug.Log("Game started");
    }

    private async void PlayClips()
    {
        Debug.Log("Play clips");
        _audioSource.PlayOneShot(_startAudioClip);


        await System.Threading.Tasks.Task.Delay(ClipDurationInMilliseconds(_startAudioClip));
        await System.Threading.Tasks.Task.Delay(500);

        foreach (var element in _gameMode.AllAnswers[0].Elements)
        {
            _audioSource.PlayOneShot(element.Value);

            await System.Threading.Tasks.Task.Delay(ClipDurationInMilliseconds(element.Value));
            await System.Threading.Tasks.Task.Delay(500);
        }

        Debug.Log("end play clips");
    }

    private int ClipDurationInMilliseconds(AudioClip clip)
    {
        var seconds = clip.length;

        var milliseconds = (int)(seconds * 1000);

        return milliseconds;
    }

    private void ProcessSelectAnswer(string value)
    {
        var rightAnswer = _gameMode.AllAnswers[0];
        var numbers = new List<int>();

        foreach(var symbol in value)
        {
            int number = 0;

            int.TryParse(symbol.ToString(), out number);

            numbers.Add(number);
        }

        int i = 0;
        foreach(var data in rightAnswer.Elements)
        {
            var key = data.Key;

            if (numbers.Count <= i)
                break;


            if(numbers[i] == key)
            {
                ProcessRightAnswer();
            }
            i++;
        }

        EndGame();
    }
}
