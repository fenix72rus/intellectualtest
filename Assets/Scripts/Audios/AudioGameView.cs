using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioGameView : GameModeView
{
    public Action<string> OnAnswerSelected;

    [SerializeField] private AudioAnswerView _audioAnswerView;

    public override void ClearAnswersPanel()
    {
    }

    public override void ClearRememberPanel()
    {
    }

    public override void EnableAnswerInteracts(bool status)
    {
    }

    public void Init(AudioTaskData audioTaskData)
    {
        _audioAnswerView.InitValue(audioTaskData);

        Init();
    }

    public override void EnableRightAnswersBorder()
    {
    }

    protected override void FillAnswersPanel()
    {
    }

    protected override void FillRememberPanel()
    {
    }

    private void Start()
    {
        _audioAnswerView.EnableInteract(true);
        _audioAnswerView.OnSelected += ProcessAnswer;
    }

    private void ProcessAnswer(string value)
    {
        _audioAnswerView.EnableInteract(false);
        OnAnswerSelected?.Invoke(value);
    }
}
